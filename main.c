#include <stdio.h>
#include <stdlib.h>

struct state
{
	

} state;

int main(int argc, char **argv)
{
	int i;

	if (argc < 2)
	{
		fprintf(stderr, "usage: %s [FILE]...\n", argv[0]);
		return EXIT_FAILURE;
	}

	for (i = 1; i < argc; i++)
	{
		void (*play)(struct state *, const char *);

		if (!detect(argv[i]))
			fprintf(stderr, "error: No codec found for \"%s\"\n", argv[i]);
	}

	return EXIT_SUCCESS;
}

