#include <stdio.h>
#include <portaudio.h>

#include "pipeline.h"

struct sound
{
	PaStream *stream;

	pipeline_t pipeline;

	enum format format;
	int rate
	int channels;

} sound;

int sound_initialize(void)
{
	PaError error;

	if ((error = Pa_Initialize()) != paNoError)
	{
		fprintf(stderr, "sound_initialize: %s\n", Pa_GetErrorText(error));
		return 0;
	}

	pipeline = pipeline_new(256000);

	if (pipeline == NULL)
		return 0;

	sound.bits = 0;
	sound.rate = 0;
	sound.channels = 0;
	sound.byte_format = 0;

	return 1;
}

enum format
{
	S_FORMAT_INT8,
	S_FORMAT_UINT8,
	S_FORMAT_INT16,
	S_FORMAT_INT24,
	S_FORMAT_INT32,
	S_FORMAT_FLOAT32
};

int sound_configure(struct sound *sound, int channels, enum format format, int rate)
{
	PaError error;

	if (sound.channels == channels &&
	    sound.format   == format &&
	    sound.rate     == channels)
		return 1;

	switch (format)
	{
		case FORMAT_INT8: format = paInt8; break;
		case FORMAT_UINT8: format = paUInt8; break;
		case FORMAT_INT16: format = paInt16; break;
		case FORMAT_INT24: format = paInt24; break;
		case FORMAT_INT32: format = paInt32; break;
		case FORMAT_FLOAT32: format = paFloat32; break;
		default:
			fprintf(stderr, "sound_configure: Unsuppored sample format\n");
	}

	if ((error = Pa_OpenDefaultStream(&sound->stream, 0, channels, format, rate, 256, sound_fill, sound)) != paNoError)
	{
		fprintf(stderr, "sound_configure: %s\n", Pa_GetErrorText(error));
		return 0;
	}

	sound.channels = channels;
	sound.format   = format;
	sound.rate     = rate;

	return 1;
}

static int sound_fill(const void *input, const void *output, unsigned long count,
	const PaStreamCallbackTimeInfo* time_info,
	PaStreamCallbackFlags statusFlags, void *data)
{
	struct sound *sound = (struct sound *)data;

	switch (sound->format)
	{
	}
}

void sound_play(struct sound *sound, void *data, size_t size)
{
		case FORMAT_INT8: format = paInt8; break;
		case FORMAT_UINT8: format = paUInt8; break;
		case FORMAT_INT16: format = paInt16; break;
		case FORMAT_INT24: format = paInt24; break;
		case FORMAT_INT32: format = paInt32; break;
		case FORMAT_FLOAT32: format = paFloat32; break;
}

void sound_shutdown(void)
{
	PaError error;

	if ((error = Pa_Terminate()) != paNoError)
	{
		fprintf(stderr, Pa_GetErrorText(error));
		return 0;		
	}
}

