#include <stdio.h>

struct sound;

struct codec
{
	const char *name;
	int (*play)(struct sound *, const char *);
	int (*test)(const char *);

} codecs[] = {
	{ "mad", mad_play, mad_test },
	{ "vorbis", vorbis_play, vorbis_test },
	{ "flac", flac_play, flac_test },

	{ NULL }
};

int play(struct sound *sound, const char *path)
{
	int i;

	for (i = 0; codecs[i].name; i++)
		if (codecs[i].test(path))
		{
			printf("play: decoder: %s\n", codecs[i].name);
			codecs[i].play(sound, path);
			return 1;
		}

	return 0;
}

