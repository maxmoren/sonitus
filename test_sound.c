#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "sound.h"

#define BUFFER_SIZE 4096

int main(void)
{
	if (!sound_init(16, 44100, 2, 1))
		return -1;

	char buffer[BUFFER_SIZE];
	ssize_t r, w;
	int l = 0;

	while ((r = read(STDIN_FILENO, buffer + l, BUFFER_SIZE - l)) > 0 || l > 0)
	{
		w = sound_buffer(buffer, r + l);
		l = r + l - w;

		if (l > 0)
			memmove(buffer, buffer + w, l);

		usleep(100);
	}

	sound_shutdown();

	return 0;
}

